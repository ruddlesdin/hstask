Paul Ruddlesdin's HS task

I am using a raspberry pi 4 for the hardware, 
where I have a MEAN stack serving static pages.
My domain name www.ruddlesdin.com is pointed to a nginx installation
on the same raspberry pi and a reverse proxy is configured to 
use the node js express server on port 8080.

I am coding using Microsoft's VS Code insiders addition and I am connected to the Raspberry Pi using a Remote SSH extension

I have used 2 API's for the task: OpenWeather and Google Maps
