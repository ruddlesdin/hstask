// Get the data payload in a JSON file
function weather( cityName ) {
    var apikey = 'afc70604836b59e5c932f5e68b119ccd';
    fetch('https://api.openweathermap.org/data/2.5/weather?q=' + cityName + '&appid=' + apikey)  
    .then(function(resp) { return resp.json() }) // Convert data to json
    .then(function(data) {
      console.log(data);
      getElements(data); // Get Weather Data Elements
  })
  .catch(function() {
    // catch any errors
  });
}

function getElements( data ) {
    var celcius = Math.round(parseFloat(data.main.temp)-273.15); // Temp is in Kelvin so -273.15
    var feels_celcius = Math.round(parseFloat(data.main.feels_like)-273.15); // Temp is in Kelvin so -273.15
    document.getElementById('description').innerHTML = data.weather[0].description;
    document.getElementById('temp').innerHTML = celcius + '&deg;';
    document.getElementById('feels').innerHTML = feels_celcius + '&deg;';
    document.getElementById('location').innerHTML = data.name;
    document.getElementById('country').innerHTML = data.sys.country;
    document.getElementById('lon').innerHTML = data.coord.lon;
    document.getElementById('lat').innerHTML = data.coord.lat;
}