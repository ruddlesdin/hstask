let map;

// Called by script before dom loaded so default coords
function initMap() {
    map = new google.maps.Map(document.getElementById("map"), {
        center: { lat: 51.51, lng: -0.13 },
        zoom: 6,
    });
}

// Overloaded and changed name for normal calls
function getMap(lon,lat) { 
    map = new google.maps.Map(document.getElementById("map"), {
        center: { lat: lat, lng: lon },
        zoom: 6,
    });
}